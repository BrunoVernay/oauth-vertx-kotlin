# Vert.x Kotlin + OAuth Example

This work is based on:
- [Vert.x Kotlin Example](https://github.com/vert-x3/vertx-examples/tree/master/kotlin-examples/web)
- [The OAuth2 auth provider Documentation](https://vertx.io/docs/vertx-auth-oauth2/kotlin/)

My aim is to use OAuth with the basic JSon API from the example. 

TODO
1. Implement OpenID Connect with KeyCloak as an IdP. 
2. Implement the Twitter example ("just" OAuth) (see Reference)


## Running

The most convenient way to run the project during development is as follows:

```
./gradlew run
```

That way, any changes to the classes will be picked up and re-deployed automatically.


### Manually

You can generate a single "shadow jar" (more on that [here](https://github.com/johnrengelman/shadow)) as follows:

```
./gradlew shadowJar
```

The jar file can now be retrieved from `./build/libs/app-shadow.jar` and deployed onto your preferred platform.

## OAuth API 

Tricky points about the Vertx's *OAuth API* :
- it extends an *AutheNtication API*!  (problem: OAuth stands for *AuthoriZation*)
- *AccessToken* extends User

Typically the `OAuth2AuthProviderImpl` as an `authenticate` function.
This function has nothing to do with *authentication*. It sends an HTTP request to the Authorization Server to get the Access Token.
After some fiddling, it can be traced back to the `getToken()` in `AbstractOAuth2Flow` class.
> This is the back channel OAuth call where the Authorization Server authenticates the *client application* (not the user!). The Client ID and Secret are provided with the Authorization code. (that i did not find)  


- There is a `OAuth2API` class that has no interface, but public `fetch()` and `makeRequest()`. No comment, no logs.
- In `AccessToken` interface, almost all functions are implemented by calling this `OAuth2API.fetch()`  
- There is a `loadJWK()` (why load/get ??) function that call the `OAuth2API.fetch()`  in `OAuth2AuthProviderImpl`.
- in `OAuth2AuthHandlerImpl`, "Auth" stands for AuthoriZation, since it extends `AuthorizationAuthHandler` but it also implements `OAuth2AuthHandler` that provides "Authentication" ...


### Network exchanges

UX: Open the Web App in browser.
Browser: GET  http://localhost:7080/ 
Set-Cookie: vertx-web.session=01...ce; Path=/


UX: click on "protected", I am redirected to Keycloack presenting a login form
Browser: GET  http://localhost:7080/protected 
Gives a 302, Location: http://localhost:8080/auth/realms/demo/protocol/openid-connect/auth?state=%2Fprotected&scope=user%3Aemail&response_type=code&client_id=vertx-web

GET  http://localhost:8080/auth/realms/demo/protocol/openid-connect/auth
- state=/protected
- scope=user:email
- response_type=code
- client_id=vertx-web
- Cookie: vertx-web.session=01...ce

Response sets 2 cookies
- Set-Cookie: AUTH_SESSION_ID=29...79
- Set-Cookie: KC_RESTART=ey...Vx8



UX: I authenticate on the Keycloack login page and I am redirected to the Web App
Browser: POST http://localhost:8080/auth/realms/demo/login-actions/authenticate
- session_code=5Q...Nio
- execution=df...9ff1
- client_id=vertx-web
- POST: username: toto et password: toto 
- Cookie: AUTH_SESSION_ID=29...79; 
- Cookie: KC_RESTART=ey...Vx8; 
- Cookie: vertx-web.session=01...ce

Gives a 302, Location: http://localhost:7080?state=%2Fprotected&session_state=29...79&code=ey...hNg
Set-Cookie: KC_RESTART=
Set-Cookie: KEYCLOAK_IDENTITY=ey..._Bc
Set-Cookie: KEYCLOAK_SESSION=demo/a1...1b/29...79


Browser: GET http://localhost:7080/
- state=/protected
- session_state=29...79
- code=ey...Ng
- Cookie: vertx-web.session=01...ce




## References

- [Tutorials / Oauth2 - Paulo Lopes](https://vertx-tutorials.jetdrone.xyz/tutorials/oauth2/github/) 
- [VertX example by Paulo Lopes](https://github.com/vert-x3/vertx-examples/tree/master/web-examples#oauth2)
- [OAuth2AuthHandler Handler - doc and code from Vertx-Web](https://vertx.io/docs/vertx-web/java/#_oauth2authhandler_handler)
- [Receiving a OAUTH2 protected resource (Twitter) - doc and code from Vertx-Web-Client](https://github.com/vert-x3/vertx-examples/tree/master/web-client-examples#receiving-a-oauth2-protected-resource)
- Obsolete: [Building Secure APIs with Vert.x and OAuth2 - Piotr Mińkowski](https://piotrminkowski.wordpress.com/2017/09/15/building-secure-apis-with-vert-x-and-oauth2/) and [Code in Git repo, branch "Security"](https://github.com/piomin/sample-vertx-microservices/blob/security/account-vertx-service/src/main/java/pl/piomin/services/vertx/account/AccountServer.java)
 
Note: there is a huge collection of Vertx examples, finding what you need is not trivial. 

Non-VertX:
- [Experimenting with Kotlin and OAuth - Scott Brady](https://www.scottbrady91.com/Kotlin/Experimenting-with-Kotlin-and-OAuth)